package com.incrementaldesign;

public class Machine {

    public int[] calculateScore(Move[] moves) {
        if(moves==null || moves[0].option ==null|| moves[1].option==null || moves.length<1 || moves[0].option.isEmpty()|| moves[1].option.isEmpty()) {
            return new int[]{};
        }
    if (moves[0].option == MoveType.COOPERATE.getType() && moves[1].option == MoveType.COOPERATE.getType()){
        return new int[] {2,2};
    }
       else if (moves[0].option == MoveType.CHEAT.getType() && moves[1].option == MoveType.CHEAT.getType()){
            return new int[] {0,0};
        }
        if (moves[0].option == MoveType.COOPERATE.getType() && moves[1].option == MoveType.CHEAT.getType()){
            return new int[] {3,-1};
        }

            return new int[] {-1,3};

    }
}
