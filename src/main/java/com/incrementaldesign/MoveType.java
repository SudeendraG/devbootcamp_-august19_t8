package com.incrementaldesign;

public enum MoveType {

    COOPERATE("Co-Operate"),
    CHEAT("Cheat");
    private  String type;

    MoveType(String type) {
    this.type=type;
    }

    public String getType() {
        return type;
    }


}
