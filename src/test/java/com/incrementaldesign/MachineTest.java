package com.incrementaldesign;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;


class MachineTest {

        Machine machine;
    @BeforeEach
    public void  initMachine() {
        machine = new Machine();
    }

    @Test
    public void shouldCalculateScore() {

        Move [] case1 = {new Move(MoveType.COOPERATE.getType()),new Move(MoveType.COOPERATE.getType())};
        Move [] case2 = {new Move(MoveType.CHEAT.getType()),new Move(MoveType.CHEAT.getType())};
        Move [] case3 = {new Move(MoveType.COOPERATE.getType()),new Move(MoveType.CHEAT.getType())};
        Move [] case4 = {new Move(MoveType.CHEAT.getType()),new Move(MoveType.COOPERATE.getType())};
        Move [] case5 = {new Move(""),new Move("")};
        Move [] case6 = {new Move(null),new Move(null)};
        assertArrayEquals(machine.calculateScore(case1), new int[]{2, 2});
        assertArrayEquals(machine.calculateScore(case2), new int[]{0, 0});
        assertArrayEquals(machine.calculateScore(case3), new int[]{3, -1});
        assertArrayEquals(machine.calculateScore(case4), new int[]{-1, 3});
        assertArrayEquals(machine.calculateScore(case5), new int[]{});
        assertArrayEquals(machine.calculateScore(case6), new int[]{});

    }
}
